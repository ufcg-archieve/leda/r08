package adt.linkedList;

public class RecursiveDoubleLinkedListImpl<T> extends
		RecursiveSingleLinkedListImpl<T> implements DoubleLinkedList<T> {

	protected RecursiveDoubleLinkedListImpl<T> previous;

	public RecursiveDoubleLinkedListImpl() {

	}

	@Override
	public void insert(T element) {
		if (element != null) {
			if (isEmpty()) {
				data = element;
				next = new RecursiveDoubleLinkedListImpl<>();
				previous = new RecursiveDoubleLinkedListImpl<>();
			} else {
				((RecursiveDoubleLinkedListImpl<T>) next).insert(element, this);
			}
		}
	}

	private void insert(T element, RecursiveDoubleLinkedListImpl<T> previousNode) {
		if (element != null) {
			if (isEmpty()) {
				data = element;
				next = new RecursiveDoubleLinkedListImpl<>();
				previous = previousNode;
			} else {
				((RecursiveDoubleLinkedListImpl<T>) next).insert(element, this);
			}
		}
	}

	@Override
	public void insertFirst(T element) {
		if (element != null) {
			if (isEmpty()) {
				insert(element);
			} else {				
				RecursiveDoubleLinkedListImpl<T> secondNode = new RecursiveDoubleLinkedListImpl<>();
				secondNode.data = data;
				secondNode.next = next;
				data = element;
				next = secondNode;
				secondNode.previous = this;
			}
		}
	}

	@Override
	public void remove(T element) {
		if (element != null && !isEmpty()) {
			if (data.equals(element)) {
				if (previous.isEmpty()) {
					removeFirst();
				} else if (next.isEmpty()) {
					removeLast();
				} else {
					previous.next = next;
					((RecursiveDoubleLinkedListImpl<T>) next).previous = previous;
				}
			} else {
				next.remove(element);
			}
		}
	}

	@Override
	public void removeFirst() {
		if (!isEmpty()) {
			if (previous.isEmpty() && next.isEmpty()) {
				data = null;
				next = null;
				previous = null;
			} else {
				data = next.data;
				next = next.next;
				((RecursiveDoubleLinkedListImpl<T>) next).previous = this;
			}
		}
	}

	@Override
	public void removeLast() {
		if (!isEmpty()) {
			if (next.isEmpty()) {
				data = null;
				next = null;
				previous = null;
			} else {
				((RecursiveDoubleLinkedListImpl<T>) next).removeLast();
			}
		}
	}

	public RecursiveDoubleLinkedListImpl<T> getPrevious() {
		return previous;
	}

	public void setPrevious(RecursiveDoubleLinkedListImpl<T> previous) {
		this.previous = previous;
	}

}
