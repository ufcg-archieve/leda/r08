package adt.linkedList;

public class DoubleLinkedListImpl<T> extends SingleLinkedListImpl<T> implements
		DoubleLinkedList<T> {

	protected DoubleLinkedListNode<T> last;

	public DoubleLinkedListImpl() {
		super.head = new DoubleLinkedListNode<>();
		this.last = new DoubleLinkedListNode<>();
	}

	@Override
	public T search(T element) {
		T result = null;
		if (element != null && !isEmpty()) {
			DoubleLinkedListNode<T> auxHead = (DoubleLinkedListNode<T>) head;
			DoubleLinkedListNode<T> auxLast = last;
			int i = 0;
			int j = size() - 1;
			while (i <= j) {
				if (auxHead.getData().equals(element) || auxLast.getData().equals(element)) {
					result = element;
					break;
				}
				auxHead = (DoubleLinkedListNode<T>) auxHead.getNext();
				auxLast = auxLast.getPrevious();
				i++;
				j--;
			}
		}
		return result;
	}

	@Override
	public void insert(T element) {
		if (element != null) {
			DoubleLinkedListNode<T> newNode = new DoubleLinkedListNode<>(element, new DoubleLinkedListNode<>(), new DoubleLinkedListNode<>());
			if (isEmpty()) {
				head = newNode;
				last = newNode;
			} else {
				last.setNext(newNode);
				newNode.setPrevious(last);
				last = newNode;
			}
		}
	}

	@Override
	public void insertFirst(T element) {
		if (element != null) {
			DoubleLinkedListNode<T> newNode = new DoubleLinkedListNode<>(element, new DoubleLinkedListNode<>(), new DoubleLinkedListNode<>());
			if (isEmpty()) {
				head = newNode;
				last = newNode;
			} else {
				((DoubleLinkedListNode<T>) head).setPrevious(newNode);
				newNode.setNext(head);
				head = newNode;
			}
		}
	}

	@Override
	public void remove(T element) {
		if (element != null && search(element) != null) {
			if (head.getData().equals(element)) {
				removeFirst();
			} else if (last.getData().equals(element)) {
				removeLast();
			} else {
				DoubleLinkedListNode<T> aux = (DoubleLinkedListNode<T>) head;
				while (!aux.getNext().isNIL()) {
					if (aux.getNext().getData().equals(element)) {
						DoubleLinkedListNode<T> removed = (DoubleLinkedListNode<T>) aux.getNext();
						aux.setNext(removed.getNext());
						((DoubleLinkedListNode<T>) removed.getNext()).setPrevious(aux);
						break;
					}
					aux = (DoubleLinkedListNode<T>) aux.getNext();
				}
			}
		}
	}

	@Override
	public void removeFirst() {
		if (!isEmpty()) {
			if (size() == 1) {
				head.setData(null);
			} else {
				head = head.getNext();
				((DoubleLinkedListNode<T>) head).setPrevious(new DoubleLinkedListNode<>());
			}
		}
	}

	@Override
	public void removeLast() {
		if (!isEmpty()) {
			if (size() == 1) {
				last.setData(null);
			} else {
				last = last.getPrevious();
				last.setNext(new DoubleLinkedListNode<>());
			}
		}
	}

	public DoubleLinkedListNode<T> getLast() {
		return last;
	}

	public void setLast(DoubleLinkedListNode<T> last) {
		this.last = last;
	}

}
