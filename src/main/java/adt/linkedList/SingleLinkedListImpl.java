package adt.linkedList;

public class SingleLinkedListImpl<T> implements LinkedList<T> {

	protected SingleLinkedListNode<T> head;

	public SingleLinkedListImpl() {
		this.head = new SingleLinkedListNode<T>();
	}

	@Override
	public boolean isEmpty() {
		return head.isNIL();
	}

	@Override
	public int size() {
		int size = 0;
		SingleLinkedListNode<T> aux = head;
		while (!aux.isNIL()) {
			size++;
			aux = aux.getNext();
		}
		return size;
	}

	@Override
	public T search(T element) {
		T result = null;
		if (element != null && !isEmpty()) {
			SingleLinkedListNode<T> aux = head;
			while (!aux.isNIL()) {
				if (aux.getData().equals(element)) {
					result = element;
					break;
				}
				aux = aux.getNext();
			}
		}
		return result;
	}

	@Override
	public void insert(T element) {
		if (element != null) {
			SingleLinkedListNode<T> newNode = new SingleLinkedListNode<>(element, new SingleLinkedListNode<>());
			if (isEmpty()) {
				head = newNode;
			} else {
				SingleLinkedListNode<T> aux = head;
				while (!aux.getNext().isNIL()) {
					aux = aux.getNext();
				}
				aux.setNext(newNode);
			}
		}
	}

	@Override
	public void remove(T element) {
		SingleLinkedListNode<T> aux = head;
		if (head.getData().equals(element)) {
			head = aux.getNext();
		} else {
			while (!aux.getNext().isNIL()) {
				if (aux.getNext().getData().equals(element)) {
					SingleLinkedListNode<T> removed = aux.getNext();
					aux.setNext(removed.getNext());
					break;
				}
				aux = aux.getNext();
			}
		}
	}

	@Override
	public T[] toArray() {
		T[] array = (T[]) new Object[size()];
		SingleLinkedListNode<T> aux = head;
		int i = 0;
		while (!aux.isNIL()) {
			array[i++] = aux.getData();
			aux = aux.getNext();
		}
		return array;
	}

	public SingleLinkedListNode<T> getHead() {
		return head;
	}

	public void setHead(SingleLinkedListNode<T> head) {
		this.head = head;
	}

}
