package adt.queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class QueueDoubleLinkedListTest {
    
    private Queue<Integer> fila;

    @Before
    public void setUp() {
        fila = new QueueDoubleLinkedListImpl<>(5);
        //fila = new CircularQueue<>(5);
        //fila = new QueueUsingStack<>(5);
    }

    public void adicionaElementosSemEncher() throws QueueOverflowException {
        fila.enqueue(7);
        fila.enqueue(13);
        fila.enqueue(16);
        fila.enqueue(-2);
    }

    @Test
    public void enqueue() {
        try {
            adicionaElementosSemEncher();
        } catch (Exception e) {
            fail();
        }
    }

    @Test(expected = QueueOverflowException.class)
    public void enqueueFilaCheia() throws Exception {
        adicionaElementosSemEncher();
        fila.enqueue(10);
        assertTrue(fila.isFull());

        fila.enqueue(15);
    }

    @Test
    public void enqueueElementoNulo() throws Exception {
        fila.enqueue(10);
        fila.enqueue(null);
        assertEquals(10, fila.head(), 0);
    }

    @Test
    public void dequeue() throws Exception {
        adicionaElementosSemEncher();
        assertEquals(7, fila.dequeue(), 0);
        assertEquals(13, fila.dequeue(), 0);
        assertEquals(16, fila.dequeue(), 0);
    }

    @Test(expected = QueueUnderflowException.class)
    public void dequeueFilaVazia() throws Exception {
        fila.dequeue();
    }

    @Test
    public void head() throws Exception {
        adicionaElementosSemEncher();
        assertEquals(7, fila.head(), 0);
        assertEquals(7, fila.head(), 0);
    }

    @Test
    public void headFilaVazia() throws Exception {
        assertNull(fila.head());
    }

    @Test
    public void isEmpty() throws Exception {
        assertTrue(fila.isEmpty());
        
        fila.enqueue(10);
        assertFalse(fila.isEmpty());
    }

    @Test
    public void isFull() throws Exception {
        assertFalse(fila.isFull());
        adicionaElementosSemEncher();
        assertFalse(fila.isFull());
        
        fila.enqueue(10);
        assertTrue(fila.isFull());
    }

}
