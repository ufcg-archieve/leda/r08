package adt.stack;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StackDoubleLinkedListTest {
    
    private Stack<Integer> pilha;

    @Before
    public void setUp() {
        pilha = new StackDoubleLinkedListImpl<>(5);
    }

    public void adicionaElementosSemEncher() throws StackOverflowException {
        pilha.push(7);
        pilha.push(13);
        pilha.push(16);
        pilha.push(-2);
    }

    @Test
    public void push() {
        try {
            adicionaElementosSemEncher();
        } catch (Exception e) {
            fail();
        }
    }

    @Test(expected = StackOverflowException.class)
    public void pushPilhaCheia() throws Exception {
        adicionaElementosSemEncher();
        pilha.push(10);
        assertTrue(pilha.isFull());

        pilha.push(15);
    }

    @Test
    public void pushElementoNulo() throws Exception {
        pilha.push(10);
        pilha.push(null);
        assertEquals(10, pilha.top(), 0);
    }

    @Test
    public void pop() throws Exception {
        adicionaElementosSemEncher();
        assertEquals(-2, pilha.pop(), 0);
        assertEquals(16, pilha.pop(), 0);
        assertEquals(13, pilha.pop(), 0);
    }

    @Test(expected = StackUnderflowException.class)
    public void popPilhaVazia() throws Exception {
        pilha.pop();
    }

    @Test
    public void top() throws Exception {
        adicionaElementosSemEncher();
        assertEquals(-2, pilha.top(), 0);
        assertEquals(-2, pilha.top(), 0);
    }

    @Test
    public void topPilhaVazia() throws Exception {
        assertNull(pilha.top());
    }

    @Test
    public void isEmpty() throws Exception {
        assertTrue(pilha.isEmpty());
        
        pilha.push(10);
        assertFalse(pilha.isEmpty());
    }

    @Test
    public void isFull() throws Exception {
        assertFalse(pilha.isFull());
        adicionaElementosSemEncher();
        assertFalse(pilha.isFull());
        
        pilha.push(10);
        assertTrue(pilha.isFull());
    }

}
