# Roteiro 08
### Lista Encadeada

- [SingleLinkedList](https://gitlab.com/ufcg-archieve/leda/r08/-/blob/4955c267ea6049ccf0e6f103fb60829c696fcce9/src/main/java/adt/linkedList/SingleLinkedListImpl.java)
- [DoubleLinkedList](https://gitlab.com/ufcg-archieve/leda/r08/-/blob/4955c267ea6049ccf0e6f103fb60829c696fcce9/src/main/java/adt/linkedList/DoubleLinkedListImpl.java)
- [RecursiveSingleLinkedList](https://gitlab.com/ufcg-archieve/leda/r08/-/blob/4955c267ea6049ccf0e6f103fb60829c696fcce9/src/main/java/adt/linkedList/RecursiveSingleLinkedListImpl.java)
- [RecursiveDoubleLinkedList](https://gitlab.com/ufcg-archieve/leda/r08/-/blob/4955c267ea6049ccf0e6f103fb60829c696fcce9/src/main/java/adt/linkedList/RecursiveDoubleLinkedListImpl.java)
- [StackDoubleLinkedList](https://gitlab.com/ufcg-archieve/leda/r08/-/blob/4955c267ea6049ccf0e6f103fb60829c696fcce9/src/main/java/adt/stack/StackDoubleLinkedListImpl.java)
- [QueueDoubleLinkedList](https://gitlab.com/ufcg-archieve/leda/r08/-/blob/4955c267ea6049ccf0e6f103fb60829c696fcce9/src/main/java/adt/queue/QueueDoubleLinkedListImpl.java)
